let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };
in with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.stable.latest.default.override { extensions = [ "rust-src" ]; })
    
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

  # compile time env variables
  INFLUXDB_HOST="http://100.89.209.13:8086";
  INFLUXDB_BUCKET="Test";
  INFLUXDB_ORG="Test";
  MQTT_ANNOUNCE_NAME = "mwtt_watchman_test";
  MQTT_HOST = "100.96.69.67";

 # RUSTFLAGS="--cfg influx_log=\"true\"";

}

