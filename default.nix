let
pkgs = import <nixpkgs> {};
in
  let
    package = { pkgs, fetchFromGitLab, stdenv, cargoSha, versionTag, ... }:
      pkgs.rustPlatform.buildRustPackage rec {
        pname = "mqtt-watchman";
        version = "${versionTag}";

        src = fetchFromGitLab {
          domain = "gitlab.com";
          owner = "seam345";
          repo = pname;
          rev = "";
          sha256 = "";
        };
        cargoSha256 = "${cargoSha}";

        # compile time env variables
       INFLUXDB_HOST="http://100.89.209.13:8086";
       INFLUXDB_BUCKET="Test";
       INFLUXDB_ORG="Test";
        MQTT_ANNOUNCE_NAME = "mwtt_watchman_test";
        MQTT_HOST = "100.96.69.67";
      };
  in
    with pkgs; {
      mqtt-watchman =
        callPackage package {
          versionTag= "v0.4";
          cargoSha = "";
        };
    }
