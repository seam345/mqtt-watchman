#[cfg(influx_log = "true")]
use futures::prelude::*;
#[cfg(influx_log = "true")]
use influxdb2::models::DataPoint;
#[cfg(influx_log = "true")]
use influxdb2::Client;
use log::{error, info};
#[cfg(not(influx_log = "true"))]
use log::{debug, trace};
#[cfg(influx_log = "true")]
use log::Level;
use rumqttc::Event;
use rumqttc::{AsyncClient, Incoming, MqttOptions, QoS};
use serde::Deserialize;
use serde_json::json;
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
#[cfg(influx_log = "true")]
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::time::Duration;

#[derive(Deserialize, Debug, Clone)]
struct Keys {
    #[serde(rename = "ifttt keys")]
    ifttt_keys: Vec<Key>,
    #[cfg(influx_log = "true")]
    #[serde(rename = "influx token")]
    influx_token: String,
}

#[derive(Deserialize, Debug, Clone)]
struct Key {
    key: String,
    prefix: String,
}

type ZigbeeTopic = String;
type Contact = bool;

// todo at some point when I can be bothered to go go the effort made the influx token a seperate file
#[tokio::main]
async fn main() {
    env_logger::init();

    let token_file = match std::env::var("TOKEN_FILE") {
        Ok(value) => value,
        Err(_) => panic!("Failed to get token file exiting"),
    };
    info!("token-file is  {}", token_file);

    let mut file = File::open(token_file).unwrap();
    let mut token: String = "".to_string();
    file.read_to_string(&mut token).unwrap();

    let keys: Keys = serde_yaml::from_str(&token).expect("unmalformed yaml config");

    #[cfg(influx_log = "true")]
    log_influx(
        "N/A".to_owned(),
        "N/A".to_owned(),
        "keys acquired".to_owned(),
        Level::Debug,
    );
    #[cfg(not(influx_log = "true"))]
    debug!("keys acquired {:?}", keys);

    let mut previous_contacts: HashMap<ZigbeeTopic, Contact> = HashMap::new();

    'main: loop {
        let mut mqttoptions = MqttOptions::new(env!("MQTT_ANNOUNCE_NAME"), env!("MQTT_HOST"), 1883);
        mqttoptions.set_keep_alive(Duration::from_secs(10));

        let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
        client
            .subscribe("zigbee2mqtt/contact/+", QoS::AtMostOnce)
            .await
            .unwrap();

        #[cfg(influx_log = "true")]
        log_influx(
            "N/A".to_owned(),
            "N/A".to_owned(),
            format!("{} connected to broker", env!("MQTT_ANNOUNCE_NAME")),
            Level::Debug,
        );
        #[cfg(not(influx_log = "true"))]
        debug!("connection to mqtt lost attempting to reconnect");

        loop {
            let notification = match eventloop.poll().await {
                Ok(notification) => notification,
                Err(err) => {
                    dbg!(err);
                    #[cfg(influx_log = "true")]
                    log_influx(
                        "N/A".to_owned(),
                        "N/A".to_owned(),
                        "Connection to mqtt lost attempting to reconnect".to_owned(),
                        Level::Error,
                    );
                    #[cfg(not(influx_log = "true"))]
                    error!("connection to mqtt lost attempting to reconnect");
                    continue 'main;
                }
            };

            match notification {
                Event::Incoming(packet) => match packet {
                    Incoming::Connect(_) => {}
                    Incoming::ConnAck(_) => {}
                    Incoming::Publish(publish) => {
                        let loggable_publish: String =
                            String::from_utf8_lossy(&publish.payload).into();

                        #[cfg(influx_log = "true")]
                        log_influx(
                            publish.topic.clone(),
                            loggable_publish.clone(),
                            "Received publish".to_owned(),
                            Level::Debug,
                        );
                        #[cfg(not(influx_log = "true"))]
                        debug!(
                            "received {}, from {}",
                            loggable_publish.clone(),
                            publish.topic
                        );

                        let current_value = std::str::from_utf8(&publish.payload)
                            .unwrap()
                            .contains("true");
                        let contact_name =
                            match publish.topic.split('/').collect::<Vec<&str>>().get(2) {
                                None => publish.topic.clone(),
                                Some(val) => val.to_string(),
                            };

                        previous_contacts
                            .entry(publish.topic.clone())
                            .and_modify(|previous_value| {
                                #[cfg(influx_log = "true")]
                                log_influx(
                                    publish.topic.clone(),
                                    loggable_publish.clone(),
                                    format!("previous value is: {}", previous_value),
                                    Level::Trace,
                                );
                                #[cfg(not(influx_log = "true"))]
                                trace!(
                                    "previous value is: {}, current is: {}",
                                    previous_value,
                                    current_value
                                );

                                if *previous_value != current_value {
                                    if current_value {
                                        send_value(
                                            &keys,
                                            format!("{} is closed", contact_name),
                                            publish.topic.clone(),
                                            loggable_publish.clone(),
                                        );
                                    } else {
                                        send_value(
                                            &keys,
                                            format!("{} is open", contact_name),
                                            publish.topic.clone(),
                                            loggable_publish.clone(),
                                        );
                                    }
                                    *previous_value = current_value;
                                }
                            })
                            .or_insert_with( || {
                                #[cfg(influx_log = "true")]
                                log_influx(
                                    publish.topic.clone(),
                                    String::from_utf8_lossy(&publish.payload).into(),
                                    "previous value was not found".to_owned(),
                                    Level::Trace,
                                );
                                #[cfg(not(influx_log = "true"))]
                                trace!("no previous value, current is: {}", current_value);
                                if current_value {
                                    send_value(
                                        &keys,
                                        format!("{} is closed (First time seen)", contact_name),
                                        publish.topic.clone(),
                                        loggable_publish.clone(),
                                    );
                                } else {
                                    send_value(
                                        &keys,
                                        format!("{} is open (First time seen)", contact_name),
                                        publish.topic.clone(),
                                        loggable_publish.clone(),
                                    );
                                }
                                current_value
                            });

                        #[cfg(influx_log = "true")]
                        log_influx(
                            publish.topic.clone(),
                            String::from_utf8_lossy(&publish.payload).into(),
                            format!("previous contact hashmap {:?}", previous_contacts),
                            Level::Trace,
                        );
                        #[cfg(not(influx_log = "true"))]
                        trace!("previous contact hashmap {:?}", previous_contacts);
                    }
                    Incoming::PubAck(_) => {}
                    Incoming::PubRec(_) => {}
                    Incoming::PubRel(_) => {}
                    Incoming::PubComp(_) => {}
                    Incoming::Subscribe(_) => {}
                    Incoming::SubAck(_) => {}
                    Incoming::Unsubscribe(_) => {}
                    Incoming::UnsubAck(_) => {}
                    Incoming::PingReq => {}
                    Incoming::PingResp => {}
                    Incoming::Disconnect => {}
                },
                Event::Outgoing(_packet) => {}
            }
        }
    }
}

fn send_value(
    keys: &Keys,
    message: String,
    mqtt_topic: String,
    mqtt_payload: String,
) {
    #[cfg(influx_log = "true")]
    log_influx(
        mqtt_topic.clone(),
        mqtt_payload.clone(),
        "iterating through sending keys and sending notification".to_owned(),
        Level::Debug,
    );
    #[cfg(not(influx_log = "true"))]
    debug!("iterating through sending keys and sending notification, for topic {}, with payload {}", mqtt_topic, mqtt_payload);
    let cloned_keys = keys.clone();
    tokio::spawn(async move {
        for key in cloned_keys.ifttt_keys {
            let client = reqwest::Client::new();
            let full_message = format!("{}{}", key.prefix, message);
            let url = format!(
                "https://maker.ifttt.com/trigger/sendNotifiaction/with/key/{}",
                key.key
            );
            let res = client
                .post(url.clone())
                .json(&json!(
                        {
                            "value1": full_message
                        }
                ))
                .send()
                .await;
            #[cfg(influx_log = "true")]
            log_influx(
                mqtt_topic.clone(),
                mqtt_payload.clone(),
                format!("url: {} payload: {}", url, full_message),
                Level::Trace,
            );
            #[cfg(not(influx_log = "true"))]
            trace!("url: {} payload: {}", url, full_message);

            #[cfg(influx_log = "true")]
            log_influx(
                mqtt_topic.clone(),
                mqtt_payload.clone(),
                format!("Response from request {:?}", res),
                Level::Debug,
            );
            #[cfg(not(influx_log = "true"))]
            debug!("Response from request {:?}", res);
        }
    });
}

#[cfg(influx_log = "true")]
static CLIENT_INSTANCE: std::sync::OnceLock<Client> = std::sync::OnceLock::new();

#[cfg(influx_log = "true")]
fn log_influx(
    mqtt_topic: String,
    mqtt_payload: String,
    message: String,
    level: Level,
) {
    tokio::spawn(async move {

        let client = CLIENT_INSTANCE.get_or_init(|| {

            let token_file = match std::env::var("TOKEN_FILE") {
                Ok(value) => value,
                Err(_) => panic!("Failed to get token file exiting"),
            };
            info!("token-file is  {}", token_file);

            let mut file = File::open(token_file).unwrap();
            let mut token: String = "".to_string();
            file.read_to_string(&mut token).unwrap();

            let keys: Keys = serde_yaml::from_str(&token).expect("unmalformed yaml config");

            let host = env!("INFLUXDB_HOST");
            let org = env!("INFLUXDB_ORG");
            Client::new(host, org, &keys.influx_token)



        });

        let Some(time_received) = SystemTime::now()
            .duration_since(UNIX_EPOCH).ok()
            else {
                error!("Unable to compute time stamp");
                error!("nothing sent to influx");
                return;
            };
        let Some(time_received): Option<i64> = time_received.as_nanos().try_into().ok() else {
            error!("unable to convert timestamp into nanos");
            error!("nothing sent to influx");
            return;
        };

        let Some(point) = DataPoint::builder("syslog")
            .tag("appname", env!("MQTT_ANNOUNCE_NAME"))
            .tag("facility", "idk")
            .tag("hostname", "boat-athena")
            .tag("host", "idk")
            .tag("mqtt_topic", mqtt_topic)
            .tag("mqtt_payload", mqtt_payload)
            .tag("severity", level_to_influx_severity(level)) // emerg, alert, crit, err, warning, notice, info, debug
            .field("facility_code", 0)
            .field("message", message)
            .field("procid", "idk")
            .field("severity_code", 0)
            .field("version", 0)
            .timestamp(time_received)
            .build().ok() else {
            error!("Failed to construct influx data");
            error!("nothing sent to influx");
            return;
        };

        match client
            .write(env!("INFLUXDB_BUCKET"), stream::iter(vec![point]))
            .await
        {
            Ok(_) => {}
            Err(err) => {
                error!("failed to send to influx, {:?}", err);
            }
        }
    });
}

#[cfg(influx_log = "true")]
fn level_to_influx_severity(level: Level) -> String {
    match level {
        Level::Error => "err".to_owned(),
        Level::Warn => "warning".to_owned(),
        Level::Info => "notice".to_owned(),
        Level::Debug => "info".to_owned(),
        Level::Trace => "debug".to_owned(),
    }
}
